var net = require("net");
var JSONStream = require('JSONStream');
var fs = require("fs");
var os = require("os");
var util = require("util");

//required args
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
//optional args
var trackName = process.argv[6];
var password = process.argv[7];
var carCount = process.argv[8] || 1;

// LOAD BOT
var defaultImpl = "base.js", botImpl = defaultImpl, implPrefix = "ud-", botPath = "./bots/";
if(botName.substr(0, implPrefix.length) === implPrefix && botName.length > implPrefix.length) {
    botImpl = botName.substr(implPrefix.length)+".js";
    var d = new Date();
    botName = (botName+"-"+d.getDate()+d.getHours()+d.getMinutes()+d.getSeconds()).substr(0, 16);
}
var botlib;
if(fs.existsSync(botPath+botImpl)) {
    botlib = require(botPath+botImpl);
} else {
    botlib = require(botPath+defaultImpl);
}
var bot = new botlib.Bot();

//console.log("BotUtils.isLeft(5) = "+botlib.BotUtils.isLeft(5));

// INIT CONTROLS
var hostname = os.hostname(), isLocal =
false &&
("Andrews-MacBook-Pro.local" === hostname || "andrews-mbp" === hostname);
var gameLog = [];

process.on("exit", function() {
    if(!isLocal) return;
    console.log(">>> Process exiting, writing file <<<");
    var filename = "./logs/"+botName+".txt";
    fs.appendFileSync(filename, new Date().toString()+"\n");
    for(var i = 0; i < gameLog.length; i++) {
        fs.appendFileSync(filename, "\n"+i+"\n");
        fs.appendFileSync(filename, util.format("received: %j", gameLog[i].rx)+"\n");
        fs.appendFileSync(filename, util.format("    sent: %j", gameLog[i].tx)+"\n");
    }
});

var controller = {
    join: function(data) {
        console.log("Joined game");
    },

    joinRace: function(data) {
        controller.join(data);
    },

    yourCar: function(data) {
        bot.yourCar(data);
    },

    gameInit: function(data) {
        bot.gameInit(data);
    },

    gameStart: function(data, tick) {
        console.log("Race started");
        bot.gameStart(data, tick);
    },

    carPositions: function(data, tick) {
        bot.updateCarState(data, tick);
        var msgType = "throttle", value;
        //try switchLane first
        value = bot.switchLane();
        if(value) {
            msgType = "switchLane";
        } else {
            //else get throttle value
            value = bot.throttle();
        }
        return value ? { msgType: msgType, data: value } : null;
    },

    crash: function(data, tick) {
        bot.crash(data, tick);
    },

    spawn: function(data, tick) {
        bot.spawn(data, tick);
    },

    lapFinished: function(data, tick) {
        bot.lapFinished(data, tick);
    },

    finish: function(data, tick) {
        bot.finish(data, tick);
    },

    gameEnd: function(data) {
        //TODO print results
        console.log("Race has ended");
        bot.gameEnd(data);
    },

    tournamentEnd: function(data) {
        console.log("Tournament end");
    },

    dnf: function(data) {
        console.log("DNF: Uh oh, either your flunked or something went wrong");
    }
}

// START IT UP
var trackStuff = "";
if(trackName) {
    trackStuff = " for track '"+trackName+"'";
    if(password) trackStuff += " (password "+password+")";
    trackStuff += " with "+(carCount - 1)+" other car(s)";
}
//console.log("Playing as "+botName+" (length "+botName.length+") and connecting to "+serverHost+":"+serverPort);
console.log((isLocal ? "[LOCAL] " : "")+"Playing "+bot.name+" as '"+botName+"' on server "+serverHost+":"+serverPort+trackStuff);

//process.exit(0);

// CONNECT TO SERVER AND RACE!
var joinType = "join", joinData = { name: botName, key: botKey };
if(trackName || carCount > 1) {
    joinType = "joinRace";
    joinData = { botId: { name: botName, key: botKey }, carCount: carCount };
    if(trackName) joinData.trackName = trackName;
    if(password) joinData.password = password;
}

client = net.connect(serverPort, serverHost, function() {
    return send({ msgType: joinType, data: joinData });
});

function send(json, received) {
    if(isLocal) gameLog.push({ rx: received, tx: json });
    client.write(JSON.stringify(json));
    return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on("data", function(payload) {
    //console.time("handle-data");
    var data = payload || {};

    /*if(data.gameTick && data.gameTick % 100 == 0) {
        console.log("Tick %d, mem: %j", data.gameTick, process.memoryUsage());
    }*/

    var response;
    if(controller[data.msgType]) {
        response = controller[data.msgType](data.data, data.gameTick);
    } else {
        console.log("Unknown msgType: "+data.msgType);
    }

    if(response) {
        send(response, payload);
    } else {
        send({ msgType: "ping", data: {} }, payload); //should we ever skip this?
    }
    //console.timeEnd("handle-data");
});

jsonStream.on("error", function() {
    var msg = "Stream error!";
    if(isLocal) gameLog.push({ rx: msg, tx: null});
    return console.log(msg);
});
