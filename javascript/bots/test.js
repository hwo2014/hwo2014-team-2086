var base = require("./base.js");

function TestBot() {
    base.Bot.call(this, { name: "TestBot" });

    this.speedMap = [];

    this.processTrack = function(pieces, lanes) {
        //go over each piece and determine optimal throttle
        var t = 1.0, prev, next;
        for(var i = 0; i < pieces.length; i++) {

        }
    }

    this.throttle = function() {
        if(!this.racing) return null;
        var cs = this.currentCarState, absAngle = Math.abs(cs.angle), t;
        if(absAngle > 40) t = 0;
        else if(absAngle > 20) t = 0.075;
        else if(absAngle > 10) t = 0.15;
        else if(absAngle > 5) t = 0.45;
        else if(absAngle > 2) t = 0.6;
        else t = 1;
        console.log("A: "+cs.angle+", T: "+t);
        return t;
    }

}

module.exports = {Bot: TestBot, BotUtils: base.BotUtils, CarState: base.CarState}
