var BotUtils = {
    lanes: []
    ,pieces: []
    ,PI: 3.141592653589793
    ,lastThrottle: 0
    ,trendMap: []

    ,isLeft: function(angleOrDfc) {
        return angleOrDfc < 0;
    }

    ,sameDirection: function(angle1, angle2) {
        return BotUtils.isLeft(angle1) === BotUtils.isLeft(angle2);
    }

    ,nextPieceIndex: function(index) {
        //return index === BotUtils.pieces.length - 1 ? 0 : index + 1;
        return index >= BotUtils.pieces.length - 1 ? index % (BotUtils.pieces.length - 1) : index + 1;
    }

    ,findNextCurve: function(pieceIndex) {
        var i = BotUtils.nextPieceIndex(pieceIndex);
        while(i !== pieceIndex) {
            if(BotUtils.pieces[i] && BotUtils.pieces[i].radius) {
                var p = BotUtils.pieces[i];
                p.index = i;
                return p;
            }
            i = BotUtils.nextPieceIndex(i);
        }
    }

    ,distance: function(piece) {
        if(!piece) return 0;
        if(piece.length) return piece.length;
        if(piece.radius && piece.angle) return (BotUtils.PI * 2 * piece.radius) * (Math.abs(piece.angle) / 360);
        return 0;
    }

    ,upcomingTrend: function(pieceIndex, howFar, includeCurrent) {
        var upcoming = { dist: 0, trend: 0 }, nextPiece, i = 0, positiveAngles = 0, negativeAngles = 0, tight = false, dist;
        if(includeCurrent) {
            i -= 1;
            howFar -= 1;
        }
        for(; i < howFar; i++) {
            nextPiece = BotUtils.pieces[BotUtils.nextPieceIndex(pieceIndex + i)];
            dist = BotUtils.distance(nextPiece);
            if(!upcoming.mid) upcoming.mid = dist / 2;
            upcoming.dist += dist;
            if(nextPiece && nextPiece.angle) {
                //upcoming.trend += nextPiece.angle
                if(nextPiece.angle > 0) positiveAngles += nextPiece.angle;
                else if(nextPiece.angle < 0) negativeAngles += nextPiece.angle;

                if(!tight) tight = nextPiece.angle > dist;
            }
        }
        upcoming.trend = positiveAngles + negativeAngles;
        upcoming.swing = positiveAngles !== 0 && negativeAngles !== 0;
        upcoming.tight = tight;
        upcoming.utd = Math.abs(upcoming.trend) / upcoming.dist;

        //var tv = tight ? 2 : Math.max(3.5, 8.5 - (10 * upcoming.utd));
        var tv = tight ? 2.5 : upcoming.trend === 0 ? 8.5 : 5.5;
        if(upcoming.swing && !tight) tv = tv / 1.5;
        upcoming.targetVelocity = tv;

        return upcoming; //dist, trend, swing, tight, utd
    }

    ,upcomingAngle: function(pieceIndex) {
        var nextCurve = BotUtils.findNextCurve(pieceIndex);
        if(!nextCurve) return 0;
        var upcoming = BotUtils.upcomingTrend(nextCurve.index, 5, false);
        var angle = nextCurve.angle;
        if(Math.abs(upcoming.trend) > Math.abs(angle) && !BotUtils.sameDirection(upcoming.trend, angle)) {
            angle *= -1;
        }
        return angle;
    }

    // ,findNextCurveBetter: function(pieceIndex) {
    //     var nextCurve = BotUtils.findNextCurve(pieceIndex);
    //     if(!nextCurve) return null;
    //
    //     //look at next 5 pieces to check for longer curve in opposite direction
    //     var i, upcoming, trend = 0;
    //     for(i = 1; i <= 5; i++) {
    //         upcoming = BotUtils.pieces[BotUtils.nextPieceIndex(nextCurve.index + i)];
    //         if(upcoming && upcoming.angle) trend += upcoming.angle;
    //     }
    //
    //     if(Math.abs(trend) > Math.abs(nextCurve.angle) && !BotUtils.sameDirection(trend, nextCurve.angle)) {
    //         nextCurve.angle *= -1;
    //     }
    //     return nextCurve;
    // }

    ,buildTrendMap: function(pieces) {
        if(!pieces) return;
        for(var i = 0; i < pieces.length; i++) {
            var upcoming = BotUtils.upcomingTrend(i, 4, true);
            BotUtils.trendMap[i] = upcoming;
            //console.log("PIECE "+i+": %j, TREND: %j", pieces[i], upcoming );
        }
    }
};

function BaseBot(cfg) {
    cfg = cfg || {};

    // META
    this.name = cfg.name || "BaseBot";
    this.defaultThrottle = cfg.defaultThrottle || 0.25;

    // STATIC RACE DATA
    this.carId = "<car name> (car color)";
    this.lanes;
    this.pieces;

    // CAR STATE
    this.racing = false;
    this.lastCarState;
    this.currentCarState;
    this.lastSwitch;

    //console.log(this.name+" constructor");

    // PRIVATE
    function carId(name, color) {
        return name+" ("+color+")";
    }

    function findCar(myCarId, carPositionsData) {
        var arr = carPositionsData || [], i, arrLength = arr.length, qCarId;
        for(i = 0; i < arrLength; i++) {
            qCarId = arr[i].id ? carId(arr[i].id.name, arr[i].id.color) : null;
            if(myCarId === qCarId) return arr[i];
        }
        return null;
    }

    // PUBLIC
    // this.getCarId = function() {
    //     return carId(this.carName, this.carColor);
    // }
    this.processTrack = function(pieces, lanes) {
        this.lanes = BotUtils.lanes = lanes;
        this.pieces = BotUtils.pieces = pieces;
        BotUtils.buildTrendMap(pieces);
    }

    // PROTOCOL INTERFACE
    this.gameInit = function(gameInitData) {
        var data = gameInitData || {};
        var trackName, pieces = [], lanes = [], numCars, numLaps;
        if(data.race) {
            if(data.race.track) {
                trackName = data.race.track.name;
                pieces = data.race.track.pieces || pieces;
                //if(data.race.track.pieces) numPieces = data.race.track.pieces.length;
                lanes = data.race.track.lanes || lanes;
                // if(lanes) {
                //     this.lanes = lanes;
                //     numLanes = data.race.track.lanes.length;
                // }
            }
            if(data.race.cars) numCars = data.race.cars.length;
            if(data.race.raceSession) numLaps = data.race.raceSession.laps;
        }
        console.log("Track '"+trackName+"' info: "+pieces.length+" pieces, "+lanes.length+" lanes, "+numCars+" cars, "+numLaps+" laps");
        this.processTrack(pieces, lanes);
    }

    this.gameStart = function(gameStartData, tick) {
        this.racing = true;
    }

    this.yourCar = function(yourCarData) {
        var data = yourCarData || {};
        this.carId = carId(data.name, data.color);
        console.log(this.name+" car is "+this.carId);
    }

    this.updateCarState = function(carPositionsData, tick) {
        //TODO reuse same 2 objects for performance ?
        //console.time("updateCarState");
        //var newCarState = new CarState(findCar(this.carId, carPositionsData), this.lastCarState, tick);
        this.lastCarState = this.currentCarState;
        //this.currentCarState = newCarState;
        this.currentCarState = new CarState(findCar(this.carId, carPositionsData), this.lastCarState, tick);
        //console.timeEnd("updateCarState");
    }

    this.switchLane = function() {
        if(!this.racing) return null;
        if(!this.currentCarState.movedPieces) return null; //don't attempt switch on very first move

        //console.log("Moved from piece "+this.lastCarState.pieceIndex+" to "+this.currentCarState.pieceIndex+" at tick "+this.currentCarState.tick);
        if(this.lastSwitch) {
            this.lastSwitch = null;
            return null;
        }
        this.lastSwitch = this.currentCarState.recommendedSwitch();
        //if(this.lastSwitch) console.log("Switching "+this.lastSwitch);
        return this.lastSwitch;
    }

    this.throttle = function() {
        if(!this.racing) return null;
        //var t;
        //return this.defaultThrottle;
        return this.currentCarState.recommendedThrottle();
        //return this.currentCarState.angleBasedThrottle();
        //return this.currentCarState.tooFastThrottle();
        //return this.currentCarState.angleBasedThrottle2();
        //return this.currentCarState.uglyThrottle();
        //return this.currentCarState.velocityBasedThrottle();
        //t = Math.max(0.0, t);
        //t = Math.min(1.0, t);
        //return t;
    }

    this.crash = function(crashData, tick) {
        this.racing = false;
        console.log("Aw, snap! "+this.name+" crashed "+this.carId+" at tick "+tick);
    }

    this.spawn = function(spawnData, tick) {
        this.racing = true;
        console.log("Ok, "+this.name+" is back in business for "+this.carId+" at tick "+tick);
    }

    this.lapFinished = function(lapData, tick) {
        var data = lapData || {};
        var log;
        if(data.car && carId(data.car.name, data.car.color) === this.carId) {
            log = "Woohoo! "+this.name+"'s car "+this.carId;
        } else {
            var cid = data.car ? carId(data.car.name, data.car.color) : "some other car";
            log = "Crappers, "+cid;
        }
        log += " completed lap";
        if(data.lapTime) log += " "+data.lapTime.lap+" in "+data.lapTime.millis+" ms";
        console.log(log);
    }

    this.finish = function(finishData, tick) {
        var data = finishData || {}, finishedCarId = carId(data.name, data.color);
        if(finishedCarId === this.carId) this.racing = false;
        console.log("Car "+finishedCarId+" finished the race at tick "+tick);
    }

    this.gameEnd = function(gameEndData) {}
}

function CarState(carData, prevCarState, tick) {
    carData = carData || {};

    //console.log("carData = %j", carData);

    //TODO do this once in processTrack, then just get lane info by index from cache
    function getLaneInfo(laneIndex, lanes) {
        var info = { dfc: 0, min: 0, max: 0 }, arr = lanes || [], i, arrLength = arr.length;
        for(i = 0; i < arrLength; i++) {
            if(!arr[i]) continue;
            if(arr[i].index === laneIndex) info.dfc = arr[i].distanceFromCenter;
            if(info.min > arr[i].distanceFromCenter) info.min = arr[i].distanceFromCenter;
            if(info.max < arr[i].distanceFromCenter) info.max = arr[i].distanceFromCenter;
        }
        return info; //dfc, min, max
    }

    this.tick = tick;
    this.angle = carData.angle || 0.0; //float

    if(carData.piecePosition) {
        this.pieceIndex = carData.piecePosition.pieceIndex; //int
        this.pieceDistance = carData.piecePosition.inPieceDistance; //float
        this.laneIndex = carData.piecePosition.lane ? carData.piecePosition.lane.endLaneIndex : -1; //int
        //this.switchingLanes = carData.piecePosition.lane ? carData.piecePosition.lane.startLaneIndex !== carData.piecePosition.lane.endLaneIndex : false; //boolean

        var nextPieceIndex = BotUtils.nextPieceIndex(this.pieceIndex);
        if(BotUtils.pieces) {
            this.piece = BotUtils.pieces[this.pieceIndex]; //Piece
            this.nextPiece = BotUtils.pieces[nextPieceIndex];
        }
        this.canSwitch = this.nextPiece && this.nextPiece.switch === true; //boolean

        this.onCurve = this.piece && this.piece.radius; //boolean

        if(BotUtils.trendMap) {
            this.trend = BotUtils.trendMap[this.pieceIndex];
            this.nextTrend = BotUtils.trendMap[nextPieceIndex];
        }
    } else {
        this.pieceIndex = -1;
        this.pieceDistance = 0.0;
        this.laneIndex = -1;
        //this.switchingLanes = false;
        this.canSwitch = false;
        this.onCurve = false;
    }

    if(prevCarState) {
        this.movedPieces = prevCarState.pieceIndex !== this.pieceIndex;
        this.angleDelta = this.angle - prevCarState.angle;
        this.lastAngle = prevCarState.angle;

        if(this.movedPieces) {
            this.tickDistance = (BotUtils.distance(prevCarState.piece) - prevCarState.pieceDistance) + this.pieceDistance;
        } else {
            this.tickDistance = this.pieceDistance - prevCarState.pieceDistance;
        }

        this.velocity = this.tickDistance / (this.tick - prevCarState.tick);
    } else {
        this.movedPieces = false;
        this.angleDelta = 0;
        this.tickDistance = 0;
        this.velocity = 0;
        this.lastAngle = 0;
    }

    // this.moved = function(prevCarState) {
    //     return prevCarState && prevCarState.pieceIndex !== this.pieceIndex;
    // }

    this.recommendedSwitch = function() {
        //if(!this.canSwitch || this.switchingLanes) return null;
        if(!this.canSwitch) return null;

        var laneInfo = getLaneInfo(this.laneIndex, BotUtils.lanes);
        var leftOfCenter = laneInfo.dfc < 0;
        //var furthestLane = (leftOfCenter && laneInfo.dfc === laneInfo.min) || (!leftOfCenter && laneInfo.dfc === laneInfo.max);
        var furthestLane = laneInfo.dfc === laneInfo.min || laneInfo.dfc === laneInfo.max;

        //var nextCurve = this.onCurve ? this.piece : BotUtils.findNextCurve(this.pieceIndex);
        //var nextCurve = BotUtils.findNextCurve(this.pieceIndex);

        //var nextCurve = BotUtils.findNextCurveBetter(this.pieceIndex);
        var angle = BotUtils.upcomingAngle(this.pieceIndex);

        //if(!nextCurve && nextCurve.angle) return null;
        if(angle === 0) return null;

        //insideCurve = (next curve has negative angle && leftOfCenter && furthestLane) || (next curve has positive angle && !leftOfCenter && furthestLane)
        //want to switch if not insideCurve
        //var insideCurve = furthestLane && ((leftOfCenter && nextCurve.angle < 0) || (!leftOfCenter && nextCurve.angle > 0));
        var insideCurve = furthestLane && BotUtils.sameDirection(angle, laneInfo.dfc);
        if(insideCurve) {
            console.log("Already inside curve");
            return null;
        }
        //var s = leftOfCenter ? "Right" : "Left";
        var s = angle > laneInfo.dfc ? "Right" : "Left";
        //console.log("Switching "+s+", p = "+this.pieceIndex+", c = %j, loc = "+leftOfCenter+", fl = "+furthestLane, nextCurve);
        //console.log("Switching "+s+", p = "+this.pieceIndex+", loc = "+leftOfCenter+", fl = "+furthestLane);
        console.log("Switching "+s+", p = "+this.pieceIndex+", dfc = "+laneInfo.dfc+", ang = "+angle);
        return s;
    }

    this.recommendedThrottle = function() {
        var minThrottle = 0.0, absAngle = Math.abs(this.angle);
        if(absAngle > 40.0) {
            //console.log("A: "+this.angle+", AD: "+this.angleDelta+", PD: "+this.pieceDistance+", CD: "+this.tickDistance+", V: "+this.velocity+", T: "+minThrottle+", get ready to crash");
            //console.log("A: "+this.angle+", V: "+this.velocity+", T: "+minThrottle+", get ready to crash");
            console.log("K: "+this.tick+", P: "+this.pieceIndex+", A: "+this.angle+", V: "+this.velocity+", UTD: "+this.trend.utd+", T: "+minThrottle+", get ready to crash");
            return minThrottle;
        }
        if(absAngle > Math.abs(this.lastAngle)) {
            minThrottle = 0.06;
            console.log("K: "+this.tick+", P: "+this.pieceIndex+", A: "+this.angle+", V: "+this.velocity+", UTD: "+this.trend.utd+", T: "+minThrottle+", drifting out");
            return minThrottle;
        }

        //keimola
        //sub 9 (8.85) second with 4 : 0.3, 10, 7.1 : 1.0, 1.1 : 60.0 : 1.7
        //sub 9 (8.83) second with 4 : 0.3, 10, 7.1 : 1.0, 1.1 : 65.0 : 1.7
        //sub 9 (8.80) second with 4 : 0.3, 10, 7.1 : 1.0, 1.105 : 65.0 : 1.7
        //sub 9 (8.78) second with 4 : 0.3, 10, 7.1 : 1.0, 1.105 : 66.0 : 1.7
        //sub 9 (8.80) second with 4 : 0.3, 10, 7.1 : 1.0, 1.105 : 67.0 : 1.7

        //germany
        //11.72 second with 4 : 0.3, 10, 7.1 : 1.0, 1.105 : 66.0 : 1.7 CRASH

        //usa
        //6.32 second with 4 : 0.3, 10, 7.1 : 1.0, 1.105 : 66.0 : 1.7

        //var upcoming = BotUtils.upcomingTrend(this.pieceIndex, 4, true); //bigger look ahead theoretically means slower approaching curves
        //var utd = Math.abs(upcoming.trend) / upcoming.dist;
        var upcoming = this.trend;
        var utd = upcoming.utd;

        var throttle;
        //if(utd < 0.3) utd = 0;
        if(utd < 0.3 && absAngle < 10 && this.velocity < 7.1) {
            throttle = 1.0;
        } else {
            throttle = upcoming.trend === 0 ? 1.0 : 1.1 - utd /*(utd / 1.0)*/; //bigger divisor means faster overall
            if(absAngle > 1.0) throttle -= (absAngle / 65.0); //bigger divisor means faster when drifting
            if(upcoming.trend !== 0) throttle -= ((this.velocity / Math.abs(upcoming.trend)) * 1.7); //bigger multiple means slower overall
        }

        throttle = Math.max(throttle, minThrottle);
        throttle = Math.min(throttle, 1.0);
        //console.log("A: "+this.angle+", AD: "+this.angleDelta+", PD: "+this.pieceDistance+", CD: "+this.tickDistance+", V: "+this.velocity+", T: "+throttle+", UTD: "+utd);
        //console.log("A: "+this.angle+", V: "+this.velocity+", T: "+throttle+", UTD: "+utd);
        console.log("K: "+this.tick+", P: "+this.pieceIndex+", A: "+this.angle+", V: "+this.velocity+", UTD: "+utd+", T: "+throttle);
        return throttle;
    }

    this.angleBasedThrottle = function() {
        var max = 1.0;
        if(this.nextPiece && this.nextPiece.angle) {
            max = Math.abs(this.nextPiece.angle) < 45 ? 0.7 : 0.5;
            if(this.piece && this.piece.length) max += 0.15;
        }

        var absAngle = Math.abs(this.angle);
        var t = absAngle / 22;

        // var t;
        // if(absAngle === 0.0) t = 0;
        // else if(absAngle < 3.0) t = 0.15;
        // else if(absAngle < 5.0) t = 0.5;
        // else if(absAngle < 7.5) t = 0.58;
        // else if(absAngle < 8.75) t = 0.79;
        // else if(absAngle < 10.0) t = 0.85;
        // else if(absAngle < 15.0) t = 0.95;
        // else if(absAngle < 20.0) t = 0.91;
        // else if(absAngle < 25.0) t = 0.991;
        // else t = 1.0;

        max = Math.max(max - t, 0);
        console.log("A: "+this.angle+", T: "+max);
        return max;
    }

    //A: -1.0776941505712117, V: 7.567894464165924, T: 0.4806139709757665, UT: -22.5, UTD: 0.059438925652687304, UD: 378.53981633974484
    //   1.0
    // - 0.01485973141317 utd/4
    //   0.98514026858683
    // - 0.50452629761106 velocity
    //   0.48061397097577

    this.tooFastThrottle = function() {
        var absAngle = Math.abs(this.angle);
        var upcoming = BotUtils.upcomingTrend(this.pieceIndex, 4, true);
        var utd = Math.abs(upcoming.trend) / upcoming.dist;

        var utdDiv, angDiv, velMul, throttle = 1.0;
        if(absAngle < 1.5) {
            utdDiv = 100.0;
            angDiv = 360.0;
            velMul = 0.0;
        } else if(absAngle < 3) {
            utdDiv = 4.0;
            angDiv = 60.0;
            velMul = 0.5;
        } else if(absAngle < 9) {
            utdDiv = 2.0;
            angDiv = 50.0;
            velMul = 1.5;
        } else if(absAngle < 15) {
            utdDiv = 1.0;
            angDiv = 40.0;
            velMul = 2.0;
        } else {
            utdDiv = 0.5;
            angDiv = 30.0;
            velMul = 3.0;
        }

        throttle -= (utd / utdDiv);
        if(absAngle > 2.5) throttle -= (absAngle / angDiv);
        if(upcoming.trend !== 0) throttle -= ((this.velocity / Math.abs(upcoming.trend)) * velMul);

        throttle = Math.max(throttle, 0.0);
        throttle = Math.min(throttle, 1.0);
        console.log("A: "+this.angle+", V: "+this.velocity+", T: "+throttle+", UT: "+upcoming.trend+", UTD: "+utd);
        return throttle;
    }

    this.uglyThrottle = function() {
        var t = 1.0, a = Math.abs(this.angle), la = Math.abs(this.lastAngle), utd = NaN;
        if(a > la) t = 0.0;
        else if(!BotUtils.sameDirection(this.angle, this.lastAngle)) t = 0.0;
        else {
            if(a === 0) t += 0.1;
            t -= (a / 65.0);
            if(a > 30) t -= 0.1;

            if(this.trend && this.nextTrend) {
                var tr = this.trend, ntr = this.nextTrend;
                utd = tr.utd; //logging
                t -= tr.utd;
                if(tr.utd < 0.3) t += 0.1;
                //if(ntr.utd > tr.utd) t -= 0.1;
                if(ntr.utd > tr.utd) t -= Math.max(0.1, ntr.utd - tr.utd);
                if(tr.utd > 0.5) t -= 0.1;
                if(ntr.utd > 0.5) t -= 0.1;

                if(tr.swing === true) t -= 0.1;
                if(ntr.swing === true) t -= 0.1;
                if(tr.tight === true) t -= 0.2;
                if(ntr.tight === true) t -= 0.1;
            }

            if(this.velocity < 4) t += 0.1;
            //else if(this.velocity > 7) t -= 0.1;
            else if(tr.trend !== 0 && this.velocity > 7) t -= (this.velocity / Math.abs(tr.trend));
        }
        t = Math.max(0.0, t);
        t = Math.min(1.0, t);
        console.log("TICK: "+this.tick+", A: "+this.angle+", V: "+this.velocity+", UTD: "+utd+", T: "+t);
        return t;
    }

    this.angleBasedThrottle2 = function() {
        var t, maxThrottle = 1.0, absTrend = NaN, utd = NaN;
        var thisAbsAngle = Math.abs(this.angle);
        var lastAbsAngle = Math.abs(this.lastAngle);

        //var upcoming = BotUtils.upcomingTrend(this.pieceIndex, 4, true);
        //utd = Math.abs(upcoming.trend) / upcoming.dist;
        var upcoming = this.trend;
        utd = upcoming.utd;

        if(thisAbsAngle === 0 && this.velocity < 4 && utd < 0.2) t = 1.0;
        //else if(thisAbsAngle > 40) t = 0.0;
        else if(thisAbsAngle > lastAbsAngle) t = 0.0;
        else if(!BotUtils.sameDirection(this.angle, this.lastAngle)) t = 0.0;
        //else if(thisAbsAngle > 40) t = 0.0;
        else if(this.velocity > 0 && (upcoming.swing === true || upcoming.tight === true)) t = 0.0;
        else {
            /*var lookAhead = this.velocity > 5 ? 3 : 2;
            var upcoming = BotUtils.upcomingTrend(this.pieceIndex, lookAhead, false), absTrend = Math.abs(upcoming.trend);
            if(absTrend >= 135) maxThrottle = 0.2;
            else if(absTrend >= 112.5) maxThrottle = 0.4;
            else if(absTrend >= 90) maxThrottle = 0.5;
            else if(absTrend >= 45) maxThrottle = 0.6;*/

            //var lookAhead = this.velocity > 7 ? 4 : 4;
            //var upcoming = BotUtils.upcomingTrend(this.pieceIndex, lookAhead, true);

            //finland 8.35
            //germany crash
            //usa 6.33

            // var upcoming = BotUtils.upcomingTrend(this.pieceIndex, 4, true);
            // utd = Math.abs(upcoming.trend) / upcoming.dist;
            if(upcoming.swing === true && upcoming.tight === true) utd *= 4; //TODO not too sure about these
            else if(upcoming.swing === true) utd *= 2;
            else if(upcoming.tight === true) utd *= 2;
            //if(this.velocity > 7) utd *= 2;

            /*if(utd < 0.1) maxThrottle = 1.0;
            else if(utd < 0.2) maxThrottle = 0.7;
            else if(utd < 0.3) maxThrottle = 0.6;
            else if(utd < 0.4) maxThrottle = 0.4; //0.6;
            else if(utd < 0.5) maxThrottle = 0.2; //0.4;
            else maxThrottle = 0.1;

            t = maxThrottle;*/

            //var max = this.velocity > 7 ? 0.8 : 1.0;
            var max = this.velocity > 6 ? 0.6 : 1.0;
            t = max - utd;

            if(thisAbsAngle > 30) t -= 0.1;
            //if(this.velocity > 7) t -= 0.1;

            t = Math.max(0.0, t);
        }

        //console.log("TCK: "+this.tick+", LA: "+this.lastAngle+", TA: "+this.angle+", AT: "+absTrend+", V: "+this.velocity+", T: "+t);
        console.log("K: "+this.tick+", P: "+this.pieceIndex+", A: "+this.angle+", V: "+this.velocity+", UTD: "+utd+", T: "+t);
        BotUtils.lastThrottle = t;
        return t;
    }

    this.velocityBasedThrottle = function() {
        var t = 1.0, a = Math.abs(this.angle), la = Math.abs(this.lastAngle), utd = NaN;
        if(a > la) t = 0.0;
        else {
            //var targetVelocity = this.pieceDistance < this.trend.mid ? this.trend.targetVelocity : this.nextTrend.targetVelocity;
            var targetVelocity = this.nextTrend.targetVelocity;
            if(this.velocity > targetVelocity) t = 0.0;
            else if(this.velocity < 2.5 && this.trend.trend === 0) t = 1.0;
            else t = targetVelocity / 12.5;
            /* utd = this.trend.utd;
            t -= utd;
            if(utd >= 0.3 || this.nextTrend >= 0.3) {
                t -= (this.velocity / 10);
            } */
        }
        t = Math.max(0.0, t);
        t = Math.min(1.0, t);
        console.log("TICK: "+this.tick+", A: "+this.angle+", V: "+this.velocity+", UTD: "+utd+", T: "+t);
        return t;
    }

}

module.exports = {Bot: BaseBot, BotUtils: BotUtils, CarState: CarState};
