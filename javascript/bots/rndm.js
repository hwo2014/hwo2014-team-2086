var base = require("./base.js");

function RndmBot() {
    base.Bot.call(this, { name: "RndmBot" });

    //console.log("BotUtils.isLeft(-45) = "+base.BotUtils.isLeft(-45));

    this.lastRandom;

    this.updateCarState = function(carPositionsData, tick) {
        if(!this.racing) return;
        this.lastRandom = Math.random();
    }

    this.switchLane = function() {
        if(!this.racing) return null;
        if(this.lastRandom > 0.8) return this.lastRandom > 0.9 ? "Left" : "Right";
    }

    this.throttle = function() {
        if(!this.racing) return null;
        return this.lastRandom > 0.4 ? this.lastRandom : this.lastRandom + 0.5;
    }
}

module.exports = {Bot: RndmBot, BotUtils: base.BotUtils, CarState: base.CarState}
